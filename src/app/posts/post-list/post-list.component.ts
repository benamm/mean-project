import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from 'rxjs';

import { Post } from "../post.model";
import { PostsService } from "../posts.service";
import { MatDialog } from "@angular/material";
import { DialogBoxComponent } from "./dialog-box.component";

@Component({
  selector: "app-post-list",
  templateUrl: "./post-list.component.html",
  styleUrls: ["./post-list.component.css"]
})
export class PostListComponent implements OnInit, OnDestroy {
  // posts = [
  //   { title: "First Post", content: "This is the first post's content" },
  //   { title: "Second Post", content: "This is the second post's content" },
  //   { title: "Third Post", content: "This is the third post's content" }
  // ];
  posts: Post[] = [];
  private postsSub: Subscription;
  isLoading = false;

  constructor(public postsService: PostsService, private dialog: MatDialog) {}

  ngOnInit() {
    /**
     * loading start
     */
    this.isLoading = true;

    this.postsService.getPosts();

    this.postsSub = this.postsService.getPostUpdateListener()
      .subscribe((posts: Post[]) => {
        /**
         * loading stop
         */
        this.isLoading = false;

        this.posts = posts;
      });
  }

  onDelete(postId: string) {
    const dialogRef = this.dialog.open(DialogBoxComponent);

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.postsService.removePost(postId);
      }
    })
  }

  ngOnDestroy() {
    this.postsSub.unsubscribe();
  }
}
