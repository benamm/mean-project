import { Component } from "@angular/core";

@Component({
  selector: "app-dialog-box",
  template: `
    <h1 mat-dialog-title>Remove Post</h1>
    <mat-dialog-content>
      <p>Are you sure remove this post?</p>
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-button [mat-dialog-close]="true">Yes</button>
      <button mat-button [mat-dialog-close]="false">no</button>
    </mat-dialog-actions>
  `
})

export class DialogBoxComponent {}
